#!/bin/bash
source utils.sh

add_crontab() {
  #             bash $USER/certbot-automation/certbot.sh subdomain  domain  email  node_red_confi_path
  line="@weekly bash $HOME/certbot-automation/certbot.sh $subdomain $domain $email $NODE_RED_CONFIG_PATH"
  (crontab -u $USER -l; echo "$line" ) | crontab -u $USER -
}

main() {
  # Check for non-root user
  check_non_root

  # check parameters supplied by user when calling the program
  if [[ "$#" == 4 ]]; then
    subdomain="$1"
    domain="$2"
    email="$3"
    NODE_RED_CONFIG_PATH="$4"

  elif [[ "$#" == 1 ]] && [[ "$1" == '--help' ]]; then
    help
    exit 1

  elif [ "$#" -lt 1 ]; then
    e_header "Certs Parameters"

    #### Domain
    parameter='Domain'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter a $parameter: "
      domain=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $domain"

    #### Subdomain
    parameter='Subdomain'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter a $parameter: "
      subdomain=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $subdomain"

    #### Email
    parameter='Email'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter an $parameter: "
      email=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $email"

    #### Node Red Config Files
    parameter='Node Red Configuration'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter the $parameter Path: "
      NODE_RED_CONFIG_PATH=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $NODE_RED_CONFIG_PATH"

  else
    e_error "Usage: $0 <subdomain> <domain> <email> <node_red_config_path>"
    e_note ""
    echo "  $0 uflow.cleantechsp sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-cleantechsp"
    exit 1
fi

add_crontab

}

#set -x   # activate debugging from here
main "$@"
#set +x   # stop debugging from here
