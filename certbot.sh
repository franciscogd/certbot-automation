#!/bin/bash
source utils.sh
HOSTNAME=$(hostname --fqdn)
packages=(
  certbot
  net-tools
)

stop_node_red() {
  # stop_node_red=$(node-red stop)
  stop_node_red=$(sudo killall -e "node-red")
  # stop_node_red=$(ps -ef | grep "node-red" | grep -v "grep" | awk '{print $2}' | sudo xargs kill)
}

renew_certs(){
  sudo certbot --agree-tos -m "$email" -d "$subdomain"."$domain" --standalone certonly -n
  #sudo certbot --agree-tos -m $3 -d $1.$2 --standalone certonly -n
  #sudo certbot --agree-tos -m "francisco.duran@sbdinc.com" -d "qa-uflow.sawstar.sbdconnect.io" --standalone certonly -n
}

link_certs(){
  local node_red_etc='/etc/node-red'

  if [ -f "$node_red_etc/fullchain.pem" ]; then
    sudo rm -rf "$node_red_etc/fullchain.pem"
  fi

  if [ ! -h "$node_red_etc/fullchain.pem" ]; then
    sudo ln -s /etc/letsencrypt/live/"$subdomain"."$domain"/fullchain.pem /etc/node-red/fullchain.pem
  fi

  if [ -f "$node_red_etc/certificate.pem" ]; then
    sudo rm -r "$node_red_etc/certificate.pem"
  fi

  if [ ! -h "$node_red_etc/certificate.pem" ]; then
    sudo ln -s /etc/letsencrypt/live/"$subdomain"."$domain"/cert.pem /etc/node-red/certificate.pem
  fi

  if [ -f "$node_red_etc/privatekey.pem" ]; then
    sudo rm -rf "$node_red_etc/privatekey.pem"
  fi

  if [ ! -h "$node_red_etc/privatekey.pem" ]; then
    sudo ln -s /etc/letsencrypt/live/"$subdomain"."$domain"/privkey.pem /etc/node-red/privatekey.pem
  fi
}

start_node_red() {
  # Start node-red in the background
  sudo node-red -u "$NODE_RED_CONFIG_PATH" &>/dev/null &
  # sudo node-red -u /opt/orchestrator-core-sef
  # sudo node-red -u /opt/orchestrator-core-gem
}

main() {
  # Check for non-root user
  check_non_root

  # check parameters supplied by user when calling the program
  if [[ "$#" == 4 ]]; then
    subdomain="$1"
    domain="$2"
    email="$3"
    NODE_RED_CONFIG_PATH="$4"

  elif [[ "$#" == 1 ]] && [[ "$1" == '--help' ]]; then
    help
    exit 1

  elif [ "$#" -lt 1 ]; then
    e_header "Certs Parameters"

    #### Domain
    parameter='Domain'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter a $parameter: "
      domain=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $domain"

    #### Subdomain
    parameter='Subdomain'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter a $parameter: "
      subdomain=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $subdomain"

    #### Email
    parameter='Email'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter an $parameter: "
      email=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $email"

    #### Node Red Config Files
    parameter='Node Red Configuration'
    e_subtitle "$parameter"
    REPLY='no' # Needed to enter while loop
    while ! is_confirmed; do
      read -p "Please enter the $parameter Path: "
      NODE_RED_CONFIG_PATH=$REPLY
      seek_confirmation "Confirm Selection"
      is_confirmed
    done
    e_success "$parameter: $NODE_RED_CONFIG_PATH"

  else
    e_error "Usage: $0 <subdomain> <domain> <email> <node_red_config_path>"
    e_note ""
    echo "  $0 uflow.cleantechsp sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-cleantechsp"
    exit 1
  fi

  e_header "Stopping Node Red"
  local stop_node_results="$(sudo fdisk -l)"
  { # 'try' block
    stop_node_red_results=$(stop_node_red)
    echo "$stop_node_red_results"
    e_success "Node Red Stopped."
    } || { # 'catch' block
    e_error " Node Red was not running."
  }

  e_header "Installing Packages"
  add_repo "ppa:certbot/certbot" # Add certbot repo
  list="$(to_install "${packages[*]}" "$(sudo "$(unix_package_manager)" list --installed)")"
  if [[ "$list" ]]; then
  for item in ${list[@]}
    do
      { # 'try' block for Centos, Fedora, RedHat, Ubuntu, Debian
        echo "$item is not on the list; installing..." && sudo "$(unix_package_manager)" -y install "$item"
        } || { # 'catch' block for Darwin (no sudo with Brew)
        echo "$item is not on the list; installing..." && "$(unix_package_manager)" -y install "$item"
      }

      # cleanup
      sudo "$(unix_package_manager)" autoremove -y
    done
  else
  e_arrow "Nothing to install."
  fi

  e_header "Renewing Certificates"
  local renew_certs_results="$(sudo fdisk -l)"
  { # 'try' block
    renew_certs_results=$(renew_certs)
    } || { # 'catch' block
    pushover "Cert Renewal Fail - $HOSTNAME" "$renew_certs_results"
    exit 126
  }

  link_certs # Soft Link certs to the appropriate node-red location

  e_header "Starting Node Red"
  local start_node_results="$(sudo fdisk -l)"
  { # 'try' block
    start_node_red_results=$(start_node_red)
    echo "$start_node_red_results"
    e_success "Node Red Started."
    } || { # 'catch' block
    pushover "Node Start Fail - $HOSTNAME" "$start_node_results"
    exit 126
  }

  pushover "Cert Renewal Success - $subdomain.$domain ($HOSTNAME)" "$renew_certs_results $start_node_red_results"
  e_success "SRE has been notified of the cert renewal."
  echo ""
}

#set -x   # activate debugging from here
main "$@"
#set +x   # stop debugging from here
