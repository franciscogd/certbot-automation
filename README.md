# Auto Certificate Renewal

This project stops node-red, renews the certificates, starts node-red, and sends a report via push notification to the SRE team subscribed to the appropriate Pushover App.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.
```
$ git clone git@gitlab.com:franciscogd/certbot-automation.git
username:
password:
```

### Prerequisites

No prerequisites, the utility installs all required packages on Centos, Ubuntu, SUSE or Darwin.
``` bash
# Install list of packages based on OS (unix only)
# [START unix_package_manager ]
function unix_package_manager() {
  local packages=$1
  UNAME=$(uname -s)
  if [[ $UNAME == 'Darwin' ]]; then
    brew install -y $packages # Darwin
  elif [[ $UNAME == 'Linux'  ]]; then
    if [ -f /etc/redhat-release ]; then
      sudo yum install -y $packages # RHEL/CentOS
    elif [ -f /etc/lsb-release ]; then
      sudo apt-get install -y $packages # Debian/Ubuntu
    elif [ -f /etc/SuSE-release ]; then
      sudo zypper install -y $packages # SUSE
    fi
  else
    e_error "Not a valid UNIX OS"
    exit 1
  fi
}
# [END unix_package_manager]
```
### Installing

After cloning the project in the desired instance, execute the `crontab.sh --help` file to view examples and learn how to use.

```
$ ./crontab.sh --help
➜ Usage:
  ./crontab.sh <subdomain> <domain> <email> <node_red_config_path>
➜ Examples:
  ./crontab.sh uflow.cleantechsp sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-cleantechsp
  ./crontab.sh dev-orchestrator sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-prototype
  ./crontab.sh uflow.gem sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem
  ./crontab.sh uflow.infrastructure sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-infra
  ./crontab.sh uflow.sawstar sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sawstar
  ./crontab.sh qa-uflow.sawstar sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sawstar
  ./crontab.sh uflow.sef sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sef
  ./crontab.sh udpbootcamp1 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem
  ./crontab.sh udpbootcamp2 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem
  ./crontab.sh udpbootcamp3 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem
  ./crontab.sh orchestrator sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-prototype
  ./crontab.sh uflow.sef-sandbox sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sef
  ./crontab.sh uflow.starterkit sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem
```

You can also run `crontab.sh` without arguments and use the prompt to supply these one by one:
```
$ ./crontab.sh

--------------------  Certs Parameters  --------------------
➜ Domain
Please enter a Domain: <domain>
Confirm Selection (y/n): y
✔ Domain: <domain>
➜ Subdomain
Please enter a Subdomain: <subdomain>
Confirm Selection (y/n): y
✔ Subdomain: <subdomain>
➜ Email
Please enter an Email: <email>
Confirm Selection (y/n): y
✔ Email: <email>
➜ Node Red Configuration
Please enter the Node Red Configuration Path: <node_red_config_path>
Confirm Selection (y/n): y
✔ Node Red Configuration: <node_red_config_path>
```

The previous block would add the following line in your crontab file:
```
@weekly bash /home/ubuntu/certbot-automation/certbot.sh <subdomain> <domain> <email> <node_red_config_path>
```

You can also run `certbot.sh` with all the parameters in one line, or following the prompts:
```
$ ./certbot.sh

--------------------  Certs Parameters  --------------------
➜ Domain
Please enter a Domain: <domain>
Confirm Selection (y/n): y
✔ Domain: <domain>
➜ Subdomain
Please enter a Subdomain: <subdomain>
Confirm Selection (y/n): y
✔ Subdomain: <subdomain>
➜ Email
Please enter an Email: <email>
Confirm Selection (y/n): y
✔ Email: <email>
➜ Node Red Configuration
Please enter the Node Red Configuration Path: <node_red_config_path>
Confirm Selection (y/n): y
✔ Node Red Configuration: <node_red_config_path>

--------------------  Stopping Node Red  --------------------

✔ Node Red Stopped.

--------------------  Installing Packages  --------------------
Reading package lists... Done
Building dependency tree       
Reading state information... Done
certbot is already the newest version (0.26.1-1+ubuntu16.04.1+certbot+2).
0 upgraded, 0 newly installed, 0 to remove and 82 not upgraded.
Reading package lists... Done
Building dependency tree       
Reading state information... Done
0 upgraded, 0 newly installed, 0 to remove and 82 not upgraded.
net-tools is not on the list; installing...
Reading package lists... Done
Building dependency tree       
Reading state information... Done
net-tools is already the newest version (1.60-26ubuntu1).
0 upgraded, 0 newly installed, 0 to remove and 82 not upgraded.
Reading package lists... Done
Building dependency tree       
Reading state information... Done
0 upgraded, 0 newly installed, 0 to remove and 82 not upgraded.

--------------------  Renewing Certificates  --------------------
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Starting new HTTPS connection (1): acme-v02.api.letsencrypt.org
Congratulations! Your certificate and chain ha been saved.

--------------------  Starting Node Red  --------------------

✔ Node Red Started.
✔ SRE has been notified of the cert renewal.

```

## Running the tests

No tests have been developed for this project.


## Deployment

```
$ git clone git@gitlab.com:franciscogd/certbot-automation.git
$ bash ~/certbot-automation/crontab.sh <subdomain> <domain> <email> <node_red_config_path>

```

## Contributing

Please help develop the [CONTRIBUTING.md] with best practices details and the process for submitting pull requests to us.

## Authors

* **Francisco Duran**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
