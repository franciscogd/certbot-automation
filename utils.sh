#!/bin/bash
# https://natelandau.com/bash-scripting-utilities/

#
# Set Colors
#

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

purple=$(tput setaf 171)
red=$(tput setaf 1)
green=$(tput setaf 76)
tan=$(tput setaf 3)
blue=$(tput setaf 38)

#
# Headers and  Logging
#

e_header() { printf "\n${bold}${purple}--------------------  %s  --------------------${reset}\n" "$@"
}
e_arrow() { printf "➜ $@\n"
}
e_success() { printf "${green}✔ %s${reset}\n" "$@"
}
e_error() { printf "${red}✖ %s${reset}\n" "$@"
}
e_subtitle() { printf "${tan}➜ %s${reset}\n" "$@"
}
e_underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
e_bold() { printf "${bold}%s${reset}\n" "$@"
}
e_note() { printf "${underline}${bold}${blue}Note:${reset}  ${blue}%s${reset}\n" "$@"
}

#
# USAGE FOR SEEKING CONFIRMATION
# seek_confirmation "Ask a question"
# Credt: https://github.com/kevva/dotfiles
#
# if is_confirmed; then
#   some action
# else
#   some other action
# fi
#

seek_confirmation() {
  printf "${bold}$@${reset}"
  read -p " (y/n): " -n 1
  printf "\n"
}

# underlined
seek_confirmation_head() {
  printf "\n${underline}${bold}$@${reset}"
  read -p "${underline}${bold} [y/n]${reset} " -n 1
  printf "\n"
}

# Test whether the result of an 'ask' is a confirmation
is_confirmed() {
if [[ $REPLY =~ ^[Yy]$ ]] || [[ $REPLY == 'yes' ]]; then
  return 0
fi
return 1
}

#
# Test whether a command exists
# Usage:
# if type_exists 'git'; then
#   e_success "Git good to go"
# else
#   e_error "Git should be installed. It isn't. Aborting."
#   exit 1
# fi

type_exists() {
if [ $(type -P $1) ]; then
  return 0
fi
return 1
}

#
# Test which OS the user runs
# if is_os "darwin"; then
#   e_success "You are on a mac"
# else
#   e_error "You are not on a mac"
#   exit 1
# fi

is_os() {
if [[ "${OSTYPE}" == $1* ]]; then
  return 0
fi
return 1
}

#
# Pushover Notifications
# Usage: pushover "Title Goes Here" "Message Goes Here"
# Credit: http://ryonsherman.blogspot.com/2012/10/shell-script-to-send-pushover.html
#

pushover () {
  PUSHOVERURL="https://api.pushover.net/1/messages.json"
  API_KEY="abq9gsnywd562hy5ezxfcqp6duksxw"
  USER_KEY="u779dkc9v58cfpswjax21fz98rdwyo"
  DEVICE="iPhoneFran"
  TITLE="${1}"
  MESSAGE="${2}"

  curl \
  -F "token=${API_KEY}" \
  -F "user=${USER_KEY}" \
  -F "device=${DEVICE}" \
  -F "title=${TITLE}" \
  -F "message=${MESSAGE}" \
  "${PUSHOVERURL}" > /dev/null 2>&1
}

#
# Given a list of desired items and installed items, return a list
# of uninstalled items. Arrays in bash are insane (not in a good way).
# Usage:
# packages=(
#   A-random-package
#   bash
#   Another-random-package
#   git
# )
# list="$(to_install "${packages[*]}" "$(brew list)")"
# if [[ "$list" ]]; then
# for item in ${list[@]}
#   do
#     echo "$item is not on the list"
#   done
# else
# e_arrow "Nothing to install.  You've already got them all."
# fi

function to_install() {
  local debug desired installed i desired_s installed_s remain
  if [[ "$1" == 1 ]]; then debug=1; shift; fi
    # Convert args to arrays, handling both space- and newline-separated lists.
    read -ra desired < <(echo "$1" | tr '\n' ' ')
    read -ra installed < <(echo "$2" | tr '\n' ' ')
    # Sort desired and installed arrays.
    unset i; while read -r; do desired_s[i++]=$REPLY; done < <(
      printf "%s\n" "${desired[@]}" | sort
    )
    unset i; while read -r; do installed_s[i++]=$REPLY; done < <(
      printf "%s\n" "${installed[@]}" | sort
    )
    # Get the difference. comm is awesome.
    unset i; while read -r; do remain[i++]=$REPLY; done < <(
      comm -13 <(printf "%s\n" "${installed_s[@]}") <(printf "%s\n" "${desired_s[@]}")
  )
  [[ "$debug" ]] && for v in desired desired_s installed installed_s remain; do
    echo "$v ($(eval echo "\${#$v[*]}")) $(eval echo "\${$v[*]}")"
  done
  echo "${remain[@]}"
}

# Install list of packages based on OS (unix only)
# [START unix_package_manager ]
function unix_package_manager() {
  # local packages=$1
  UNAME=$(uname -s)
  if [[ $UNAME == 'Darwin' ]]; then
    # macOS -> in macOS "Homebrew as root is extremely dangerous and no longer supported"
    # brew install -y $packages
    echo 'brew'
  elif [[ $UNAME == 'Linux'  ]]; then
    if [ -f /etc/redhat-release ]; then
      # RHEL/CentOS
      # sudo yum install -y $packages
      echo 'yum'
    elif [ -f /etc/lsb-release ]; then
      # Debian/Ubuntu
      # sudo apt-get install -y $packages
      echo 'apt'
    elif [ -f /etc/SuSE-release ]; then
      # SUSE
      # sudo zypper install -y $packages
      echo 'zypper'
    fi
  else
    e_error "Not a valid UNIX OS"
    exit 1
  fi
}
# [END unix_package_manager]

# [START check_non_root ]
check_non_root(){
  if [[ $(id -u) == 0 ]]; then
    e_error "Re-run this script as a non-root user with sudo privileges."
    exit 1
  else
    user=$(whoami)
  fi
}
# [END check_non_root]

# install repo
add_repo(){
  local repo="$1"
  { # 'try' block for Ubuntu, Debian
    sudo add-"$(unix_package_manager)"-repository -y "$repo"
  } || { # 'catch' block for Fedora, Rhel, Centos
    sudo "$(unix_package_manager)"-config-manager --add-repo "$repo"
  }
  sudo "$(unix_package_manager)" update -y
}

# [START help ]
help() {
  e_subtitle "Usage:"
  echo "  $0 <subdomain> <domain> <email> <node_red_config_path>"
  e_subtitle "Examples:"
  echo "  $0 uflow.cleantechsp sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-cleantechsp"
  echo "  $0 dev-orchestrator sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-prototype"
  echo "  $0 uflow.gem sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem"
  echo "  $0 uflow.infrastructure sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-infra"
  echo "  $0 uflow.sawstar sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sawstar"
  echo "  $0 qa-uflow.sawstar sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sawstar"
  echo "  $0 uflow.sef sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sef"
  echo "  $0 udpbootcamp1 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem"
  echo "  $0 udpbootcamp2 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem"
  echo "  $0 udpbootcamp3 sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-gem"
  echo "  $0 orchestrator sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-prototype"
  echo "  $0 uflow.sef-sandbox sbdconnect.io francisco.duran@sbdinc.com /opt/orchestrator-core-sef"
}
# [END help]
